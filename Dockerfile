FROM mhart/alpine-node:latest

# Create the app directory and set as working directory
RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

# Copy local files to container's working directory
COPY . /usr/src/app

# Install node packages
RUN npm install --quiet

# Expose port 8080 from the container to the host
EXPOSE 8080

CMD ["npm", "run", "start"]
